# Bike App

This is a social app that allows cyclist to interact with each other.

All the screenshots for the app is in the [appscreenshot folder](./appscreenshot)

### Before Installation

Before installation, make sure you have React Native CLI set up.
Please refer [here](https://reactnative.dev/docs/environment-setup)

### Installation

Install the dependencies and devDependencies.

```sh
$ cd bikeapp
$ npm install
```

Run the app (android)

```sh
$ npx react-native run-android
```

Run the app (ios)

```sh
$ npx react-native run-ios
```
