import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';


// HomeScreen Content
import HomeContent from './HomeScreenContent/HomeContent';
import MapContent from './HomeScreenContent/MapContent';
import MessageContent from './HomeScreenContent/MessageContent';
import EventContent from './HomeScreenContent/EventContent';

export default class HomeScreen extends React.Component {
    state = {
        contentType: "home",
    }

    renderContent() {
        if (this.state.contentType == "home") {
            return (
                <HomeContent navigation={this.props.navigation}/>
            )
        }
        if (this.state.contentType == "map") {
            return (
                <MapContent />
            )
        }
        if (this.state.contentType == "message") {
            return (
                <MessageContent navigation={this.props.navigation}/>
            )
        }
        if (this.state.contentType == "event") {
            return (
                <EventContent navigation={this.props.navigation}/>
            )
        }
    }

    renderAddButton() {
        if (this.state.contentType == "home") {
            return (
                <TouchableOpacity style={styles.addButtonContent} onPress={()=>this.goToNewPost()}>
                    <Image source={require("../assets/pencil.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            )
        }
        if (this.state.contentType == "event") {
            return (
                <TouchableOpacity style={styles.addButtonContent} onPress={()=>this.goToNewEvent()}>
                    <Image source={require("../assets/calendar_plus.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            )
        }
    }

    goToNewEvent(){
        this.props.navigation.navigate("CreateEvent");
    }

    goToNewPost(){
        this.props.navigation.navigate("CreatePost");
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView style={{ backgroundColor: "#dddddd" }}>
                    {/* Header */}
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                            <Image source={require("../assets/menu_bar.png")} style={{ width: 35, height: 35 }} />
                        </TouchableOpacity>
                        <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Bike App</Text>
                        <Image source={require("../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                    </View>
                    {/* Content */}
                    <View style={styles.body}>
                        {this.renderContent()}
                    </View>
                </ScrollView>

                {/* Footer */}
                <View style={styles.footer}>
                    <TouchableOpacity onPress={() => this.setState({ contentType: "home" })}>
                        <Image source={require("../assets/home_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ contentType: "event" })}>
                        <Image source={require("../assets/calendar.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ contentType: "map" })}>
                        <Image source={require("../assets/map_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ contentType: "message" })}>
                        <Image source={require("../assets/message_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                </View>

                {/* Add Button */}
                <View style={styles.addButtonContainer}>
                    {this.renderAddButton()}
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    body: {
        marginBottom: 80,
    },
    footer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        paddingBottom: 6,
        paddingTop: 6,
        // position:"absolute",
        // bottom:0,
    },
    addButtonContainer: {
        position: "absolute",
        bottom: 80,
        right: 10,
    },
    addButtonContent: {
        backgroundColor: "#2c3039",
        borderRadius: 20,
        padding: 15,
    }
})