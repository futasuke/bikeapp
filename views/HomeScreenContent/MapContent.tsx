import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    ImageBackground,
    Dimensions
} from 'react-native';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class MapContent extends React.Component {
    render() {
        return (
            <ImageBackground source={require("../../assets/wallpaper/dummymap.jpg")} style={{ width: screenWidth, height: screenHeight - 185 }} resizeMode="cover">
                <View style={{flex:1}}>
                    <TouchableOpacity style={[styles.userMarker,{top:50,left:50}]}>
                        <Image source={require("../../assets/bicycle.png")} style={{ width: 20, height: 20 }} resizeMode="contain" />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.userMarker,{top:100,right:50}]}>
                        <Image source={require("../../assets/bicycle.png")} style={{ width: 20, height: 20 }} resizeMode="contain" />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.userMarker,{bottom:100,left:100}]}>
                        <Image source={require("../../assets/bicycle.png")} style={{ width: 20, height: 20 }} resizeMode="contain" />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

const styles =StyleSheet.create({
    userMarker:{
        backgroundColor:"#2c3039",
        position:"absolute",
        padding:10,
        borderRadius:20,
    }
})