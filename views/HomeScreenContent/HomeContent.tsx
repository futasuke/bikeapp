import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";

import ActivityModal from '../ActivityModal';

// HomeContent Components
import Post from './HomeContentComponent/Post';

const dummyJson = require("./dummyPost.json");

@inject('store')
@observer
export default class HomeContent extends React.Component {
    state = {
        postdata: [],
        showspinner:false,
    }

    componentDidMount() {
        // console.log(this);
        let self = this;
        this.getPostData();
        this._onFocus = this.props.navigation.addListener('focus', () => {
            self.getPostData();
        });
    }

    componentWillUnmount(){
        this._onFocus();
    }

    getPostData() {
        let self = this;
        this.setState({showspinner:true})
        axios.get(this.props.store.ip + "/graphql?query={postAll{id,content,created_at,user{fullname,picturePath},comment{content,user{fullname}}}}")
            .then(function (response) {
                // console.log(response);
                self.setState({showspinner:false})
                self.setState({ postdata: response.data.data.postAll });
                // self.checkState();
            }).catch(function (error) {
                self.setState({showspinner:false})
                console.log(error);
            })
    }

    checkState() {
        console.log(this.state.postdata);
    }

    showSpinnerModal(){
        if(this.state.showspinner==true){
            return(
                <ActivityModal />
            )
        }
    }

    renderPost() {
        let self = this;

        return this.state.postdata.map(function (data, index) {
            return (
                <Post key={index} postData={data} navigation={self.props.navigation}/>
            )
        })
    }

    render() {
        return (
            <View style={styles.contentContainer}>
                
                {/* Activity Indicator */}
                {this.showSpinnerModal()}

                {this.renderPost()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        alignItems: "center"
    },
    postContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 10,
        borderRadius: 20,
        flexDirection: "row"
    }
})