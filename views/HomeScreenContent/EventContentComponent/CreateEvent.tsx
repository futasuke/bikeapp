import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions,
    Alert,
} from 'react-native';
import axios from 'axios';

// Mobx
import { observer, inject } from "mobx-react";

@inject('store')
@observer
export default class CreateEvent extends React.Component {
    state = {
        title: "",
        description: "",
        location: "",
        start_time: "",
        start_date: "",
        fee: "",
    }

    createEvent() {
        console.log(this.state)

        let self = this;

        const query = `mutation{createEvent(input:{
            owner_id:${this.props.store.userData.id},
            title:"${this.state.title}",
            description:"""${this.state.description}""",
            start_time:"${this.state.start_time}",
            start_date:"${this.state.start_date}",
            fee:"${this.state.fee}",location:"${this.state.location}"
        }){id title description}}`

         axios.post(this.props.store.ip + "/graphql", {
            query: query,
        }, { headers: { 'Content-Type': 'application/json' } }).then(function (response) {
            console.log(response);
            // self.props.navigation.goBack();
        }).catch(function (error) {
            console.log(error);
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dddddd" }}>

                    {/* Header */}
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={require("../../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                        </TouchableOpacity>
                        <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Create Event</Text>
                        <TouchableOpacity style={styles.postButton} onPress={() => this.createEvent()}>
                            <Text style={{ color: "#2c3039", fontWeight: "bold" }}>CREATE</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Content */}
                    <View style={{ marginTop: 20 }}>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Title</Text>
                            <View style={styles.cardContent}>
                                <TextInput placeholder="Event title" style={{ padding: 0 }} onChangeText={(value)=>this.setState({title:value})}/>
                            </View>
                        </View>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Description</Text>
                            <View style={styles.cardContent}>
                                <TextInput placeholder="Event description" style={{ padding: 0 }} multiline={true} onChangeText={(value)=>this.setState({description:value})}/>
                            </View>
                        </View>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Event Location</Text>
                            <View style={styles.cardContent}>
                                <TextInput placeholder="Event location" style={{ padding: 0 }} onChangeText={(value)=>this.setState({location:value})}/>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row" }}>
                            <View style={[styles.cardContainer, { flex: 5 }]}>
                                <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Event Time</Text>
                                <View style={styles.cardContent}>
                                    <TextInput placeholder="Event location" style={{ padding: 0 }} onChangeText={(value)=>this.setState({start_time:value})}/>
                                </View>
                            </View>
                            <View style={[styles.cardContainer, { flex: 5 }]}>
                                <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Event Date</Text>
                                <View style={styles.cardContent}>
                                    <TextInput placeholder="Event location" style={{ padding: 0 }} onChangeText={(value)=>this.setState({start_date:value})}/>
                                </View>
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
                            <View style={[styles.cardContainer]}>
                                <Text style={{ fontSize: 18, color: "grey", fontWeight: "bold" }}>Event Fee</Text>
                                <View style={styles.cardContent}>
                                    <TextInput placeholder="Event Fee (Enter 0 for no fee (free) )" style={{ padding: 0 }} onChangeText={(value)=>this.setState({fee:value})}/>
                                </View>
                            </View>
                        </View>

                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    postButton: {
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
    },
    cardContainer: {
        // backgroundColor: "white", 
        padding: 10,
    },
    cardContent: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        marginTop: 10,
    }
})