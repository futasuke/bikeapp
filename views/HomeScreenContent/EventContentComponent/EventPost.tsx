import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';

export default class EventPost extends React.Component {

    componentDidMount() {
        // console.log(this.props);
    }

    goToDetail(){
        this.props.navigation.navigate("EventDetail",{postData:this.props.postData});
    }

    render() {
        return (
            <TouchableOpacity style={{ width: "90%" }} onPress={()=>this.goToDetail()}>
                <View style={styles.postContainer}>
                    <View style={{marginBottom:20}}>
                        <Image source={require("../../../assets/wallpaper/cycling.jpg")} style={{width:"100%",height:120}} borderRadius={20}/>
                    </View>
                    <Text style={{ fontWeight: "bold", fontSize: 18 }}>{this.props.postData.title}</Text>
                    <View style={styles.descriptionContainer}>
                        <Text style={{color:"grey"}} numberOfLines={2} ellipsizeMode={"tail"}>{this.props.postData.description}</Text>
                    </View>
                </View>
                {/* Footer */}
                <View style={styles.footerContainer}>
                    <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <Image source={require("../../../assets/clock.png")} style={{ width: 30, height: 30 }} />
                        <Text style={{color:"grey"}}>{this.props.postData.start_time}</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <Image source={require("../../../assets/calendar_dark.png")} style={{ width: 30, height: 30 }} />
                        <Text style={{color:"grey"}}>{this.props.postData.start_date}</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <Image source={require("../../../assets/dollarsign.png")} style={{ width: 30, height: 30 }} />
                        <Text style={{color:"grey"}}>{this.props.postData.fee==0?"Free":this.props.postData.fee}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    postContainer: {
        backgroundColor: "white",
        padding: 10,
        // flexDirection: "row",
        marginTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingBottom: 20,
    },
    footerContainer: {
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 5,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        paddingLeft: 10,
        paddingRight: 10,
        borderTopColor: "grey",
        borderTopWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
    },
    descriptionContainer:{
        marginTop: 20,
        // height:20,
    }
})