import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    TextInput,
    RefreshControl,
    Keyboard,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";
import ActivityModal from '../../ActivityModal';

@inject('store')
@observer
export default class ConversationScreen extends React.Component {
    state = {
        messages: [],
        sendmessage: "",
        dummyState: 1,
        refreshing: false,
        textInput: React.createRef(),
        showspinner: false,
    }

    componentDidMount() {
        // console.log(this.props)
        this.getMessages()
    }

    // componentDidUpdate(){
    //     this.getMessages()
    // }

    getMessages() {
        let self = this;
        this.setState({ showspinner: true })
        axios.get(this.props.store.ip + `/graphql?query={chatroomdetail(id:${this.props.route.params.roomId}){message{user{id fullname picturePath}message created_at}}}`)
            .then(function (response) {
                // console.log(response);\
                self.setState({ showspinner: false })
                self.setState({ messages: response.data.data.chatroomdetail.message })
            }).catch(function (error) {
                self.setState({ showspinner: false })
                console.log(error);
            })
    }

    refreshContent() {
        this.getMessages();
    }

    sendMessage() {
        let self = this;
        Keyboard.dismiss();
        this.state.textInput.current.clear();
        // const query = `mutation{createPost(input:{user_id:${this.props.store.userData.id},content:"""${this.state.content}"""}){id content user_id}}`
        const query = `mutation{
            createMessage(input:{chatroom_id:${this.props.route.params.roomId},user_id:${this.props.store.userData.id},message:"""${this.state.sendmessage}"""}){
              id
              message
            }
        }`

        axios.post(this.props.store.ip + "/graphql", {
            query: query,
        }, { headers: { 'Content-Type': 'application/json' } }).then(function (response) {
            console.log(response);
            // self.props.navigation.goBack();
            self.setState({ dummyState: self.state.dummyState + 1 })
            self.getMessages();
        }).catch(function (error) {
            console.log(error);
        })
    }

    showSpinnerModal() {
        if (this.state.showspinner == true) {
            return (
                <ActivityModal />
            )
        }
    }

    renderMessage() {
        let self = this

        function renderChatBubble(data) {
            console.log(data)
            if (data.user.id == self.props.store.userData.id) {
                return (
                    <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
                        <View style={{ backgroundColor: "#2c3039", marginTop: 15, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, padding: 20 }}>
                            <Text style={{ color: "white" }}>{data.message}</Text>
                        </View>
                        <View style={{marginLeft:6}}>
                            <Image source={{uri:self.props.store.ip+"/"+data.user.picturePath, cache: 'reload'}} style={{ width: 50, height: 50, borderRadius:25 }} />
                        </View>
                    </View>
                )
            } else {
                return (
                    <View style={{ flexDirection: "row" }}>
                        <View style={{marginRight:6}}>
                            <Image source={{uri:self.props.store.ip+"/"+data.user.picturePath, cache: 'reload'}} style={{ width: 50, height: 50, borderRadius:25 }} />
                        </View>
                        <View style={{ backgroundColor: "white", marginTop: 15, borderTopRightRadius: 20, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, padding: 20 }}>
                            <Text>{data.message}</Text>
                        </View>
                    </View>
                )
            }
        }

        return this.state.messages.map(function (data, index) {
            // console.log(data);
            return (
                <View key={index} style={{}}>
                    {renderChatBubble(data)}
                </View>
            )
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {/* Activity Indicator */}
                {this.showSpinnerModal()}

                {/* Header */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require("../../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>{this.props.route.params.fullname}</Text>
                    <Image source={require("../../../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                </View>

                {/* Content Scrollview */}
                <ScrollView
                    style={{ backgroundColor: "#dddddd" }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.refreshContent()}
                        />
                    }

                    keyboardShouldPersistTaps={"always"}>



                    {/* Content */}
                    <View style={styles.contentContainer}>
                        {this.renderMessage()}
                    </View>
                </ScrollView>

                <View style={styles.footerContainer}>
                    <TextInput placeholder="New message..." multiline={true} style={{ flex: 14 }} onChangeText={(value) => this.setState({ sendmessage: value })} ref={this.state.textInput} />
                    <TouchableOpacity style={{ flex: 2 }} onPress={() => { this.sendMessage() }}>
                        <Image source={require("../../../assets/send.png")} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    contentContainer: {
        // backgroundColor:"white",
        padding: 10,
        marginTop: 15,
    },
    footerContainer: {
        flexDirection: "row",
        alignItems: "center",
        padding: 10
    }
})