import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
import EventPost from './EventContentComponent/EventPost';
import axios from 'axios';
import {observer,inject} from "mobx-react";
import ActivityModal from '../ActivityModal';

@inject('store')
@observer
export default class EventContent extends React.Component{
    state={
        allEvent:[],
        showspinner:false,
    }

    componentDidMount(){
        let self = this;
        this.getEvent();
        this._onFocus = this.props.navigation.addListener('focus', () => {
            self.getEvent();
        });
        // this.getEvent();
    }

    componentWillUnmount(){
        this._onFocus();
    }

    getEvent(){
        let self = this;
        this.setState({showspinner:true})
        axios.get(this.props.store.ip+"/graphql?query={eventAll{id,title,description,start_time,start_date,fee,location,user{fullname},totalParticipant}}")
        .then(function(response){
            // console.log(response)
            self.setState({showspinner:false})
            self.setState({allEvent:response.data.data.eventAll})
        }).catch(function(error){
            self.setState({showspinner:false})
            console.log(error);
        })
    }

    showSpinnerModal(){
        if(this.state.showspinner==true){
            return(
                <ActivityModal />
            )
        }
    }

    renderPost(){
        let self = this;

        return this.state.allEvent.map(function(data,index){
            return(
                <EventPost key={index} postData={data} navigation={self.props.navigation}/>
            )
        })
    }

    render(){
        return(
            <View style={{flex:1,alignItems:"center"}}>

                {/* Activity Indicator */}
                {this.showSpinnerModal()}

                {this.renderPost()}
            </View>
        )
    }
}