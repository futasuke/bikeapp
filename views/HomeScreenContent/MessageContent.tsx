import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";
import ActivityModal from '../ActivityModal';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

const dummyMessageList = require("./dummyMessageList.json");
const dummyFriendList = require("./dummyFriendList.json");

@inject('store')
@observer
export default class MessageContent extends React.Component {
    state = {
        contentType: "inbox",
        messageList: [],
        showspinner: false,
    }

    componentDidMount() {
        // console.log(dummyFriendList);
        // console.log(this.props);
        this._onFocus = this.props.navigation.addListener('focus', () => {
            this.getChatRoom();
        });
        this.getChatRoom();
    }

    componentWillUnmount() {
        this._onFocus();
    }

    getChatRoom() {
        let self = this;
        // ${this.props.store.userData.id}
        this.setState({ showspinner: true })
        axios.get(this.props.store.ip + `/graphql?query={chatroomone(userone_id:${this.props.store.userData.id}){id usertwo{fullname picturePath}message{message created_at}}chatroomtwo(usertwo_id:${this.props.store.userData.id}){id userone{fullname picturePath}message{message created_at}}}`)
            .then(function (response) {
                // console.log(response);
                // console.log(response.data.data.chatroomone);
                let tmpArr = response.data.data.chatroomone.concat(response.data.data.chatroomtwo);
                // console.log(tmpArr);
                self.setState({ showspinner: false })
                self.setState({ messageList: tmpArr });
            }).catch(function (error) {
                self.setState({ showspinner: false })
                console.log(error);
            })
    }

    goToConversation(roomId, fullname) {
        this.props.navigation.navigate("ConversationScreen", { roomId, fullname });
    }

    showSpinnerModal() {
        if (this.state.showspinner == true) {
            return (
                <ActivityModal />
            )
        }
    }

    renderMessageList() {
        let self = this;
        return this.state.messageList.map(function (data, index) {
            // console.log(data);
            let latest_conversation = "";
            let convertedTime = "";
            if (data.message.length != 0) {
                latest_conversation = data.message[data.message.length - 1];
                convertedTime = new Date(latest_conversation.created_at).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: true });
            }

            return (
                <TouchableOpacity key={index} style={[styles.perListContainer, { marginTop: index != 0 ? 20 : 0 }]} onPress={() => self.goToConversation(data.id, data.userone == null ? data.usertwo.fullname : data.userone.fullname)}>
                    <View style={{ marginRight: 10, flex: 2 }}>
                        <Image source={{uri:self.props.store.ip+"/"+ (data.userone==null?data.usertwo.picturePath:data.userone.picturePath), cache: 'reload'}} style={{ width: 40, height: 40, borderRadius:20 }} />
                    </View>
                    <View style={{ flex: 12 }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <Text style={{ fontWeight: "bold", fontSize: 16 }}>{data.userone == null ? data.usertwo.fullname : data.userone.fullname}</Text>
                            <Text style={{ color: "grey", fontSize: 12 }}>{convertedTime}</Text>
                        </View>

                        <View style={{ marginTop: 10 }}>
                            <Text style={{ color: "grey" }}>{latest_conversation != "" ? latest_conversation.message : latest_conversation}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    renderFriendList() {
        return dummyFriendList.map(function (data, index) {
            return (
                <TouchableOpacity key={index} style={[styles.perListContainer, { marginTop: index != 0 ? 20 : 0 }]}>
                    <View style={{ marginRight: 10 }}>
                        <Image source={require("../../assets/profile.png")} style={{ width: 40, height: 40 }} />
                    </View>
                    <View>
                        <Text style={{ fontWeight: "bold", fontSize: 16 }}>{data.name}</Text>
                        <Text style={{ color: "grey", marginTop: 5 }}>@{data.username}</Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    renderContent() {
        if (this.state.contentType == "inbox") {
            return (
                <View style={{ alignItems: "center" }}>
                    {this.renderMessageList()}
                </View>
            )
        }
        if (this.state.contentType == "friend") {
            return (
                <View style={{ alignItems: "center" }}>
                    {this.renderFriendList()}
                </View>
            )
        }
    }

    render() {
        return (
            <View>
                {/* Activity Indicator */}
                {this.showSpinnerModal()}
                
                {/* Nav bar */}
                <View>
                    <View style={{ backgroundColor: "#2c3039", width: screenWidth, height: 50 }}></View>
                    <View style={{ width: screenWidth, height: 30 }}></View>
                    <View style={styles.navBarContainer}>
                        <TouchableOpacity style={styles.navBarButton} onPress={() => this.setState({ contentType: "inbox" })}>
                            <Image source={require("../../assets/message_dark.png")} style={{ width: 30, height: 30 }} />
                            <Text>Inbox</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.navBarButton} onPress={() => this.setState({ contentType: "friend" })}>
                            <Image source={require("../../assets/friend_dark.png")} style={{ width: 30, height: 30 }} />
                            <Text>Friends</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* Content */}
                <View style={{ marginTop: 30 }}>
                    {this.renderContent()}
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    navBarContainer: {
        position: "absolute",
        bottom: 0,
        // backgroundColor:"pink",
        width: screenWidth,
        flexDirection: "row",
        justifyContent: "space-evenly",
    },
    navBarButton: {
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
    },
    perListContainer: {
        backgroundColor: "white",
        width: "90%",
        flexDirection: "row",
        padding: 10,
        borderRadius: 10,
    }
})