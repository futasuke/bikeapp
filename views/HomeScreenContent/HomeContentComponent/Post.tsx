import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import { observer, inject } from "mobx-react";

@inject('store')
@observer
export default class Post extends React.Component {
    state = {
        commentList:[],
        showComment: false,
        commentContent:"",
        textInput: React.createRef(),
    }

    componentDidMount() {
        // console.log("Post props");
        console.log(this.props);
        this.getComments();
    }

    getComments(){
        let self = this;
        
        axios.get(this.props.store.ip+`/graphql?query={getPostComment(id:${this.props.postData.id}){comment{content user{fullname picturePath}}}}`)
        .then(function(response){
            self.setState({commentList:response.data.data.getPostComment.comment})
        }).catch(function(error){
            console.log(error);
        })
    }

    createComment(){
        this.state.textInput.current.clear();
        let self = this;
        const query = `mutation{createComment(input:{user_id:${this.props.store.userData.id},content:"""${this.state.commentContent}""",post_id:${this.props.postData.id}}){id content user_id}}`

        axios.post(this.props.store.ip+"/graphql",{
            query: query,
        }, { headers: { 'Content-Type': 'application/json' } }).then(function(response){
            console.log(response);
            self.getComments();
        }).catch(function(error){
            console.log(error);
        })
    }

    renderComment() {
        let self = this;
        if (this.state.showComment == true) {

            let commentList = this.state.commentList.map(function (data, index) {
                return (
                    <View style={{ flexDirection: "row", marginTop: index != 0 ? 15 : 0 }} key={index}>
                        <View style={{marginRight:6}}>
                            <Image source={{uri:self.props.store.ip+"/"+data.user.picturePath, cache: 'reload'}} style={{ width: 40, height: 40, borderRadius:20 }} />
                        </View>
                        <View style={styles.commentBubble}>
                            <View>
                                <Text style={{ fontWeight: "bold", color: "#2c3039" }}>{data.user.fullname}</Text>
                            </View>
                            <View style={{ marginTop: 5 }}>
                                <Text style={{ lineHeight: 25 }}>{data.content}</Text>
                            </View>
                        </View>
                    </View>
                )
            })

            return (
                <View style={styles.commentContainer}>

                    {/* Render comment list */}
                    {commentList}

                    {/* Reply comment */}
                    <View style={{ flexDirection: "row", marginTop: 20 }}>
                        <View style={{ marginRight: 20, flex: 1, }}>
                            <TouchableOpacity>
                                <Image source={require("../../../assets/camera.png")} style={{ width: 20, height: 20, marginBottom: 10 }} />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require("../../../assets/image.png")} style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>
                        <TextInput placeholder="Write a comment..." style={{ padding: 0, flex: 8 }} multiline={true} onChangeText={(value)=>this.setState({commentContent:value})} ref={this.state.textInput}/>
                        <View style={{ flex: 2, justifyContent: "center", alignItems: "center" }}>
                            <TouchableOpacity onPress={()=>this.createComment()}>
                                <Image source={require("../../../assets/send.png")} style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={{ width: "90%" }}>
                <View style={styles.postContainer}>
                    <View style={{marginRight:6}}>
                        {/* <Text>Profile Picture</Text> */}
                        <Image source={{uri:this.props.store.ip+"/"+this.props.postData.user.picturePath, cache: 'reload'}} style={{ width: 40, height: 40, borderRadius:20 }} />
                    </View>
                    <View>
                        <Text style={{ fontWeight: "bold", color: "#2c3039" }}>{this.props.postData.user.fullname}</Text>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ lineHeight: 25 }}>{this.props.postData.content}</Text>
                        </View>
                    </View>
                </View>
                {/* Footer */}
                <View style={[styles.footerContainer, {
                    borderBottomLeftRadius: this.state.showComment == false ? 20 : 0,
                    borderBottomRightRadius: this.state.showComment == false ? 20 : 0,
                }]}>
                    <TouchableOpacity onPress={() => { if (this.state.showComment == false) { this.setState({ showComment: true }) } else { this.setState({ showComment: false }) } }}>
                        <Image source={require("../../../assets/comments.png")} style={{ width: 20, height: 20 }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require("../../../assets/like.png")} style={{ width: 20, height: 20 }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image source={require("../../../assets/share.png")} style={{ width: 20, height: 20 }} />
                    </TouchableOpacity>
                </View>

                {/* Comment */}
                {this.renderComment()}


            </View>
        )
    }
}

const styles = StyleSheet.create({
    postContainer: {
        backgroundColor: "white",
        padding: 10,
        flexDirection: "row",
        marginTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    footerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        justifyContent: "space-evenly",
        padding: 5,
    },
    commentContainer: {
        backgroundColor: "white",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 10,
    },
    commentBubble: {
        backgroundColor: "#dddddd",
        padding: 9,
        borderRadius: 20,
    }
})