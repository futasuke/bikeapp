import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";

@inject('store')
@observer
export default class CreatePost extends React.Component {
    state = {
        content: ""
    }

    newPost() {
        // console.log(this.state);
        // console.log(this.props.store.userData);

        let self = this;

        const query = `mutation{createPost(input:{user_id:${this.props.store.userData.id},content:"""${this.state.content}"""}){id content user_id}}`

        axios.post(this.props.store.ip + "/graphql", {
            query: query,
        }, { headers: { 'Content-Type': 'application/json' } }).then(function (response) {
            console.log(response);
            self.props.navigation.goBack();
        }).catch(function (error) {
            console.log(error);
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dddddd" }}>

                    {/* Header */}
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={require("../../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                        </TouchableOpacity>
                        <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Create Post</Text>
                        <TouchableOpacity style={styles.postButton} onPress={() => this.newPost()}>
                            <Text style={{ color: "#2c3039", fontWeight: "bold" }}>POST</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Content */}
                    <View style={{ marginTop: 20 }}>
                        <View style={styles.cardContainer}>

                            <View style={{ marginRight: 15 }}>
                                <Image source={require("../../../assets/profile.png")} style={{ width: 45, height: 45 }} />
                            </View>

                            <View>
                                <View>
                                    <Text style={{ fontWeight: "bold", fontSize: 16 }}>{this.props.store.userData.fullname}</Text>
                                </View>

                                <View style={{ marginTop: 10 }}>
                                    <TextInput placeholder="What's on your mind?" multiline={true} onChangeText={(value) => this.setState({ content: value })} />
                                </View>

                            </View>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    postButton: {
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
    },
    cardContainer: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        flexDirection: "row",
    }
})