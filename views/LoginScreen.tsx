import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions,
    Alert,
} from 'react-native';
import axios from 'axios';

// Mobx
import { observer, inject } from "mobx-react";
import ActivityModal from './ActivityModal';

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

// For upper background purpose
const ratio = screenWidth / 1080;

@inject('store')
@observer
export default class LoginScreen extends React.Component {
    state = {
        formType: "login",
        email: "",
        username: "",
        password: "",
        retype_password: "",
        showspinner: false,
    }

    componentDidMount() {
        // console.log(this.props);
    }

    login() {
        if (this.state.email == "" || this.state.password == "") {
            Alert.alert("Please enter all required fields.");
            return;
        } else {
            let self = this;
            this.setState({showspinner:true});

            axios.post(this.props.store.ip + "/api/login", {
                email: this.state.email,
                password: this.state.password
            }).then(function (response) {
                console.log(response)
                self.setState({showspinner:false});
                if (response.data.errors) {
                    console.log("error");
                    return;
                } else {
                    console.log("success");
                    self.props.store.userData = response.data.data;
                    self.props.navigation.navigate("HomeDrawer");
                    return;
                }
            }).catch(function (error) {
                console.log(error)
                self.setState({showspinner:false});
            })
        }
    }

    register() {
        // Check state not empty
        if (this.state.email == "" || this.state.password == "" || this.state.retype_password == "") {
            Alert.alert("Please enter all required fields.");
            return;
        } else if (this.state.password != this.state.retype_password) {
            Alert.alert("Password and retype password is not the same.");
            return;
        } else {
            let self = this;
            this.setState({showspinner:true});

            axios.get(this.props.store.ip + `/graphql?query=mutation{createUser(username:"${this.state.username}",email:"${this.state.email}",password:"${this.state.password}",fullname:"${this.state.username}"){id,username,email,fullname,picturePath}}`)
                .then(function (response) {
                    console.log(response);
                    self.setState({showspinner:false});
                    if (response.data.errors) {
                        console.log("error");
                        return;
                    } else {
                        console.log("success");
                        self.props.store.userData = response.data.data.createUser
                        self.props.navigation.navigate("HomeDrawer");
                    }
                }).catch(function (error) {
                    console.log(error);
                    self.setState({showspinner:false});
                })
        }
        // this.props.navigation.navigate("HomeDrawer");
    }

    showSpinnerModal(){
        if(this.state.showspinner==true){
            return(
                <ActivityModal/>
            )
        }
    }

    renderContent() {
        if (this.state.formType == "login") {
            return (
                <View style={{ width: "100%", alignItems: "center" }}>
                    <View style={[styles.inputContainer]}>
                        <Image source={require("../assets/email.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Email" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ email: value })} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Password" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ password: value })} />
                    </View>
                    <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]} onPress={() => { this.login() }}>
                        <Text style={{ color: "white", fontWeight: "bold" }}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        if (this.state.formType == "register") {
            return (
                <View style={{ width: "100%", alignItems: "center" }}>
                    <View style={[styles.inputContainer]}>
                        <Image source={require("../assets/email.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Email" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ email: value })} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/user_dark.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Username" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ username: value })} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Password" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ password: value })} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Re-enter Password" style={{ padding: 0, flex: 13 }} onChangeText={value => this.setState({ retype_password: value })} />
                    </View>
                    <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]} onPress={() => { this.register() }}>
                        <Text style={{ color: "white", fontWeight: "bold" }}>REGISTER</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dddddd" }}>

                    {/* Activity indicator */}
                    {this.showSpinnerModal()}

                    <View style={styles.upperPart}>
                        <Image source={require("../assets/loginbg_upper.png")} style={{ width: screenWidth, height: 736 * ratio }} resizeMode={"contain"} />
                    </View>

                    <View style={styles.lowerPart}>
                        <View style={styles.tabContainer}>
                            <TouchableOpacity onPress={() => { this.setState({ formType: "login" }) }}>
                                <Text style={{ fontWeight: "bold" }}>LOGIN</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ formType: "register" }) }}>
                                <Text style={{ fontWeight: "bold" }}>REGISTER</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.contentContainer}>
                            {this.renderContent()}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    upperPart: {
        backgroundColor: "#2c3039",
    },
    lowerPart: {
        backgroundColor: "#dddddd",
        marginBottom: 20,
    },
    tabContainer: {
        height: 50,
        // backgroundColor: "red",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        borderBottomColor: "#2c3039",
        borderBottomWidth: 1,
    },
    contentContainer: {
        flex: 9,
        paddingTop: 50,
        // backgroundColor:"blue"
    },
    inputContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 20,
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: "center",
    },
    submitButton: {
        backgroundColor: "#2c3039",
        width: "90%",
        borderRadius: 20,
        padding: 15,
        justifyContent: "center",
        alignItems: "center",
    }
})