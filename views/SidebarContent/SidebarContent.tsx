import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
} from 'react-native';

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

// For upper background purpose
const ratio = screenWidth / 1080;

export default class SidebarContent extends React.Component {
    componentDidMount() {
        // console.log(this)
    }

    goTo(place){
        this.props.navigation.navigate(place);
    }

    signOut(){
        this.props.navigation.navigate("Login");
    }

    render() {
        return (
            <View>
                {/* Upper Part */}
                <ImageBackground source={require("../../assets/sidebarbg_upper.png")} style={{ width: "100%", height:736 * ratio * 0.8 }} resizeMode="contain">
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingTop: 20 }}>
                        <Image source={{uri:this.props.store.ip+"/"+this.props.store.userData.picturePath, cache: 'reload'}} style={{ width: 100, height: 100, borderRadius:50 }} />
                        <Text style={{ color: "white", fontWeight: "bold", letterSpacing: 2 }}>{this.props.store.userData.fullname}</Text>
                    </View>
                </ImageBackground>
                {/* Lower Part */}
                <View>

                    <TouchableOpacity style={styles.listContainer} onPress={()=>this.goTo("Home")}>
                        <Image source={require("../../assets/home_dark.png")} style={{ width: 40, height: 40 }} />
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Home</Text>
                        <Image source={require("../../assets/chevron_right.png")} style={{ width: 40, height: 40 }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.listContainer} onPress={()=>this.goTo("Setting")}>
                        <Image source={require("../../assets/setting.png")} style={{ width: 40, height: 40 }} />
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Setting</Text>
                        <Image source={require("../../assets/chevron_right.png")} style={{ width: 40, height: 40 }} />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.listContainer} onPress={()=>this.signOut()}>
                        <Image source={require("../../assets/signout.png")} style={{ width: 40, height: 40 }} />
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Sign Out</Text>
                        <Image source={require("../../assets/chevron_right.png")} style={{ width: 40, height: 40 }} />
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listContainer: {
        borderBottomWidth: 1,
        borderBottomColor: "grey",
        padding: 10,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    }
})