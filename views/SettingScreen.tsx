import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';

export default class SettingScreen extends React.Component {

    goToEditProfile(){
        this.props.navigation.navigate("EditProfile")
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dddddd" }}>

                    {/* Header */}
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                            <Image source={require("../assets/menu_bar.png")} style={{ width: 35, height: 35 }} />
                        </TouchableOpacity>
                        <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Settings</Text>
                        <Image source={require("../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                    </View>

                    {/* Body */}
                    <View style={{ marginBottom: 80, alignItems: "center" }}>
                        {/* Main Setting */}
                        <View style={[styles.cardContainer]}>

                            <TouchableOpacity style={styles.cardButton} onPress={()=>this.goToEditProfile()}>
                                <Image source={require("../assets/user_dark.png")} style={{ width: 35, height: 35 }} />
                                <Text>Edit Profile</Text>
                                <Image source={require("../assets/chevron_right.png")} style={{ width: 35, height: 35 }} />
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.cardButton,{marginTop:20}]}>
                                <Image source={require("../assets/password.png")} style={{ width: 35, height: 35 }} />
                                <Text>Change Password</Text>
                                <Image source={require("../assets/chevron_right.png")} style={{ width: 35, height: 35 }} />
                            </TouchableOpacity>
                        </View>

                        {/* App info */}
                        <View style={[styles.cardContainer]}>
                            <Text>App Version: 1.0.0</Text>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    cardContainer: {
        marginTop: 20,
        backgroundColor: "white",
        width: "90%",
        padding: 10,
        borderRadius: 20,
    },
    cardButton: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    }
})