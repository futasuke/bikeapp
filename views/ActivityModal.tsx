import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    Modal,
    ActivityIndicator
} from 'react-native';
// import { observer, inject } from "mobx-react";

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

// @inject('store')
// @observer
export default class ActivityModal extends React.Component {
    render() {
        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={true}
            >
                <View style={{ height: screenHeight, width: screenWidth, backgroundColor: "rgba(0,0,0,0.4)", justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'white', width: 150, height: 150, justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}>
                        <ActivityIndicator size="large" animating={true} color="#491d74" />
                    </View>
                </View>
            </Modal>
        )
    }
}