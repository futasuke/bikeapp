import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
} from 'react-native';
import {observer,inject} from "mobx-react";
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import { values } from 'mobx';
import {launchImageLibrary} from 'react-native-image-picker';

@inject('store')
@observer
export default class EditProfile extends React.Component {
    state={
        email:this.props.store.userData.email,
        username:this.props.store.userData.username,
        fullname:this.props.store.userData.fullname,
        id:this.props.store.userData.id,
        base64:"",
        filetype:"",
    }

    componentDidMount(){
        console.log(this.props);
    }

    openGallery(){
        const options={
            mediaType:'photo',
            includeBase64:true,
        }

        launchImageLibrary(options,(response)=>{
            console.log(response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
                return;
            }
            let tmpType = response.type.split("/");
            this.setState({filename:response.fileName,filetype:tmpType[1],base64:response.base64});
            this.uploadImage();
        })
    }

    uploadImage(){
        let self = this;
        axios.post(this.props.store.ip+"/api/upload-profile-image",{
            base64:this.state.base64,
            id:this.state.id,
            filetype:this.state.filetype
        }).then(function(response){
            self.updateUser()
        }).catch(function(error){
            console.log(error.response);
        })
    }

    updateUser(){
        let self = this;

        axios.get(this.props.store.ip+`/graphql?query=mutation{updateUser(id:${this.state.id},username:"${this.state.username}",fullname:"${this.state.fullname}",email:"${this.state.email}"){id,username,email,fullname,picturePath}}`)
        .then(function(response){
            console.log(response)
            if (response.data.errors) {
                console.log("error");
                return;
            } else {
                console.log("success");
                self.props.store.userData = response.data.data.updateUser;
                self.props.navigation.goBack();
            }
        }).catch(function(error){
            console.log(error)
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dddddd" }}>

                    {/* Header */}
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={require("../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                        </TouchableOpacity>
                        <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Edit Profile</Text>
                        <Image source={require("../../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                    </View>

                    {/* Body */}
                    <View>
                        {/* Profile Picture */}
                        <View style={styles.upperPart}>
                            <Image source={{uri:this.props.store.ip+"/"+this.props.store.userData.picturePath, cache: 'reload'}} style={{ width: 100, height: 100, borderRadius:50 }} />
                            <TouchableOpacity onPress={()=>this.openGallery()}>
                                <Text style={{fontWeight:"bold",color:"white"}}>Change Picture</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Profile Data */}
                        <View style={styles.lowerPart}>

                            <View style={styles.cardContainer}>
                                <Text style={{fontWeight:"bold"}}>Email :</Text>
                                <TextInput style={{color:"grey",padding:0}} defaultValue={this.state.email} onChangeText={value=>this.setState({email:value})}/>
                            </View>

                            <View style={styles.cardContainer}>
                                <Text style={{fontWeight:"bold"}}>Username :</Text>
                                <TextInput style={{color:"grey",padding:0}} defaultValue={this.state.username} onChangeText={value=>this.setState({username:value})}/>
                            </View>

                            <View style={styles.cardContainer}>
                                <Text style={{fontWeight:"bold"}}>Full Name :</Text>
                                <TextInput style={{color:"grey",padding:0}} defaultValue={this.state.fullname} onChangeText={value=>this.setState({fullname:value})}/>
                            </View>

                            <TouchableOpacity style={styles.submitButton} onPress={()=>this.updateUser()}>
                                <Text style={{fontWeight:"bold",color:"white"}}>UPDATE</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    upperPart: {
        backgroundColor: "#2c3039",
        justifyContent:"center",
        alignItems:"center",
        paddingBottom:20,
    },
    lowerPart: {
        alignItems:"center",
        // paddingTop:20,
    },
    cardContainer:{
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:20,
        paddingRight:20,
        backgroundColor:"white",
        width:"90%",
        borderRadius:20,
        flexDirection:"row",
        justifyContent:"space-between",
        marginTop:20,
        alignItems:"center"
    },
    submitButton:{
        width:"90%",
        backgroundColor: "#2c3039",
        marginTop:40,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:20,
        padding:20,
    }
})