import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Settings
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

// Mobx
import { Provider } from "mobx-react";
import AppStore from './mobxstate/store';
const store = window.store = new AppStore();

// Views / Screens
import LoginScreen from './views/LoginScreen';
import HomeScreen from './views/HomeScreen';
import SidebarContent from './views/SidebarContent/SidebarContent';
import SettingScreen from './views/SettingScreen';
import EditProfile from './views/SettingScreenContent/EditProfile';
import CreatePost from './views/HomeScreenContent/HomeContentComponent/CreatePost';
import CreateEvent from './views/HomeScreenContent/EventContentComponent/CreateEvent';
import EventDetail from './views/HomeScreenContent/EventContentComponent/EventDetail';
import ConversationScreen from './views/HomeScreenContent/MessageContentComponent/ConversationScreen';

declare const global: { HermesInternal: null | {} };

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
const MainStack = createStackNavigator();
const MainDrawer = createDrawerNavigator();

export default class App extends React.Component {
  mainDrawer(props) {
    let self = this;
    let thisRoute = props.route;

    return (
      <Provider store={store}>
        <MainDrawer.Navigator drawerContent={props => { return (<SidebarContent navigation={props.navigation} route={thisRoute} store={store}/>) }} drawerStyle={{ width: screenWidth * 0.8 }}>
          <MainDrawer.Screen name="Home" component={HomeScreen} />
          <MainDrawer.Screen name="Setting" component={SettingScreen} />
        </MainDrawer.Navigator>
      </Provider>
    )
  }

  componentDidMount(){
    // console.log(store);
  }

  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <MainStack.Navigator headerMode="none">
            <MainStack.Screen name="Login" component={LoginScreen} />
            <MainStack.Screen name="HomeDrawer" component={this.mainDrawer} />
            <MainStack.Screen name="EditProfile" component={EditProfile} />
            <MainStack.Screen name="CreatePost" component={CreatePost} />
            <MainStack.Screen name="CreateEvent" component={CreateEvent} />
            <MainStack.Screen name="EventDetail" component={EventDetail} />
            <MainStack.Screen name="ConversationScreen" component={ConversationScreen} />
          </MainStack.Navigator>
        </NavigationContainer>
      </Provider>
    )
  }
}
